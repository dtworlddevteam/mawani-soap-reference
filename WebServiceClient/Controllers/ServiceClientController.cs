﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Web.Http;
using System.Xml;
using VesselWebServiceClient.VesselService;
using NLog;
using Newtonsoft.Json;
using System.Text;

namespace VesselWebServiceClient.Controllers
{
    [RoutePrefix("serviceclient")]
    public class ServiceClientController : ApiController
    {
        #region Consuming the web service
        private static Logger logger = LogManager.GetCurrentClassLogger();
        [HttpPost, Route("xmldata")]
        public IHttpActionResult InvokeService([FromBody] XmlElement xElement)
        {
            var logMessage = new StringBuilder();
            if (xElement == null)
                return Content(HttpStatusCode.BadRequest, "Error in Xml input data format.");

            else
            {
                logMessage.Append("Method:" + Request.Method + Environment.NewLine+"Url:"+Request.RequestUri + Environment.NewLine);
                var endPointAddress = ConfigurationManager.AppSettings["EndPointAddress"];
                B2BPortClient b2BPortClient = new B2BPortClient("B2BPortSoap11", new EndpointAddress(endPointAddress));
                b2BPortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["VUserName"];
                b2BPortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["VPassword"];
                using (new OperationContextScope(b2BPortClient.InnerChannel))
                {
                    OperationContext.Current.OutgoingMessageHeaders.Add(
                         new SecurityClass("UsernameToken-49", b2BPortClient.ClientCredentials.UserName.UserName, b2BPortClient.ClientCredentials.UserName.Password));
                    b2bDataRequest b2BData = new b2bDataRequest
                    {
                        data = xElement.OuterXml
                    };
                    b2bDataResponse b2BDataResponse;
                    try
                    {
                        b2BDataResponse = b2BPortClient.b2bData(b2BData);                        
                    }
                    catch (EndpointNotFoundException exc)
                    {
                        logMessage.Append("Error:" + exc.InnerException + Environment.NewLine + "Request:" + b2BData.data);
                        logger.Error(logMessage);
                        return Content(HttpStatusCode.BadGateway, exc.Message);
                    }
                    catch (Exception ex)
                    {
                        logMessage.Append("Error:" + ex.InnerException.Message + Environment.NewLine + "Request:" + b2BData.data);
                        logger.Error(logMessage);
                        return Content(HttpStatusCode.BadRequest, ex.InnerException.Message);
                    }
                    logMessage.Append("Success:" + "Request : " + b2BData.data + Environment.NewLine + "Response : " +
                        JsonConvert.SerializeObject(b2BPortClient.b2bData(b2BData).responseText));
                    logger.Info(logMessage);
                    return Ok(b2BDataResponse.response);

                }
            }
        }
        #endregion
    }
}
