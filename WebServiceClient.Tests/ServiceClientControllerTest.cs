﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http.Results;
using System.Xml;
using VesselWebServiceClient.Controllers;

namespace VesselWebServiceClient.Tests
{
    [TestClass]
    public class ServiceClientControllerTest
    {
        ServiceClientController controller;
        public ServiceClientControllerTest()
        {
            controller= new ServiceClientController();
        }
        [TestMethod]
        public void MigrateData_InputXml_ReturnsSuccess()
        {            
            XmlElement xmlInput = GetElement();
            var result = controller.InvokeService(xmlInput) as OkNegotiatedContentResult<string>;         
            Assert.AreEqual("Success", result.Content);            
        }

        [TestMethod]
        public void MigrateData_EndPointFailure_ReturnsFailure()
        {           
            XmlElement xmlInput = GetElement();
            var result = controller.InvokeService(xmlInput) as NegotiatedContentResult<string>;
            Assert.AreEqual("Error: Endpoint connection issue.", result.Content);
        }

        private XmlElement GetElement()
        {
            string xmlSchema =
                "<envelope><header><senderId>dfgfdg</senderId><receiverId>dfgdg</receiverId>" +
                "<controlRefNo>123456</controlRefNo><dateTimeFormat>yyyyMMdd</dateTimeFormat>" +
                "<messageCount>1</messageCount><freeText>CDV</freeText>" +
                "<documentDateTime>201711221010</documentDateTime><messageType>CDV</messageType>" +
                "<messageId>WCP-CDV-027</messageId><version>1.0</version>" +
                "<functionCode>15</functionCode><security><checksum/><signature/>" +
                "</security></header><messages><containerDetailsView><containerNo>dfg</containerNo>" +
                "<location>df</location></containerDetailsView><containerDetailsView><containerNo>dfgdfgf</containerNo>" +
                "<location>df</location></containerDetailsView><containerDetailsView><containerNo>cvbcb</containerNo>" +
                "<location>cvb</location></containerDetailsView></messages></envelope>";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlSchema);
            return doc.DocumentElement;
        }
       
    }
}
